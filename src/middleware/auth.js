const jwt = require('jsonwebtoken');

module.exports = {
  checkAuth: (token, callback) => {
    if (!token) {
      return callback({ auth: false, message: 'No existe el token en header.' });
    }
    return jwt.verify(token, process.env.AUTH0_SECRET, (err) => {
      if (err) {
        return callback({ auth: false, message: 'No autorizado para ésta operación. Token inválido.' });
      }
      return callback({ auth: true, message: 'ok' });
    });
  }
};
