const UtilsCTRL = require('../controllers/UtilsController');
const Auth = require('../middleware/auth');

module.exports = {
  //GetAll Users
  getUsers: (ctx, cb) => {
    const User  = ctx.orm().user;
    return User.findAll().then((users) => {
      return cb(users);
    })
    .catch( (err) => {
      UtilsCTRL.error(ctx, 500, 'Error al obtener usuarios', cb);
    });
  },
  //getByEmailName for login
  getUsersByEmail: (ctx, cb) => {
    switch (UtilsCTRL.validateEmail(ctx.request.body.email)) {
      case true:
      const User  = ctx.orm().user;
      return User.findAll({ where: { email: ctx.request.body.email, name: ctx.request.body.name } }).then( (user)=> {
        return cb(user, ctx);
      })
      .catch( (err) => {
        UtilsCTRL.error(ctx, 500, 'Error en consulta', cb);
      });
      break;
      default:
      UtilsCTRL.error(ctx, 422, 'Email Inválido', cb);
      break;
    }
  },
  //Save a new User
  save: (ctx, cb) => {
    switch (UtilsCTRL.validateEmail(ctx.request.body.email)) {
      case true:
      const User  = ctx.orm().user;
      return User.create(ctx.request.body).then( (users)=> {
        return cb(users);
      }).catch( (err) => {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        return cb(null);
      });
      break;
      default:
      UtilsCTRL.error(ctx, 422, 'Email Inválido', cb);
      break;
    }
  },

  //update an user
  update: async (ctx, id, cb) => {

    return Auth.checkAuth(ctx.request.headers['x-access-token'], function (token) {

      if (UtilsCTRL.validateEmail(ctx.request.body.email) && token.auth === true) {
        const User = ctx.orm().user;
        return User.update(ctx.request.body, { where: { 'id': id } }).then((user) => {
          return cb(user, ctx);
        });
      }
      else if (UtilsCTRL.validateEmail(ctx.request.body.email) && token.auth === false) {
        return UtilsCTRL.error(ctx, 401, token.message, cb);
      }
      else {
        return UtilsCTRL.error(ctx, 422, 'Email Inválido', cb);
      }
    });
  },
  //Delete an user
  delete: (ctx, id, cb) => {
    const User  = ctx.orm().user;
    return User.destroy({where:{ 'id': id }}).then((result) => {
      return cb(result, ctx);
    })
    .catch((err) => {
      ctx.status = err.status || 500;
      ctx.body = err.message;
      return cb(null, ctx);
    });
  }
};
