import Router from 'koa-router';

const UserCTRL = require('../controllers/UserController');

const router = new Router();

// User router

router.get('/users', async (ctx, next) => {
  try {
    return UserCTRL.getUsers(ctx, (users) => {
      ctx.body = {
        data: users
      };
      return next();
    });
  } catch (err) {
    return next();
  }
});

router.post('/users', async (ctx, next) => {
  try {
    return UserCTRL.save(ctx, (user) => {
      ctx.body = {
        data: user
      };
      return next();
    });
  } catch (err) {
    return next();
  }
});

router.put('/users/:id', async (ctx, next) => {
  try {
    const id = ctx.params.id;
    return UserCTRL.update(ctx, id, (user) => {
      ctx.body = {
        data: user
      };
      return next();
    });
  } catch (err) {
    return next();
  }
});
router.delete('/users/:id', async (ctx, next) => {
  try {
    const id = ctx.params.id;
    return UserCTRL.delete(ctx, id, (result) => {
      ctx.body = {
        data: result
      };
      return next();
    });
  } catch (err) {
    return next();
  }
});
router.get('/sync', async (ctx) => {
  const sq = ctx.orm();
  const User = ctx.orm().user;
  sq.sync({ force: true })
  .then(() => User.create({
    name: 'Chuck',
    email: 'chuck@mail.com',
    birthday: new Date(1980, 6, 20)
  }));
  ctx.status = 200;
});
const routes = router.routes();
export default () => routes;
