import Router from 'koa-router';

const UserCTRL = require('../controllers/UserController');

const jwt = require('jsonwebtoken');

const router = new Router();
router.post('/users/login', async (ctx, next) => {
  return (UserCTRL.getUsersByEmail(ctx, (user) => {
    if (user.length > 0) {
      const tokenValue = jwt.sign({ id: user.id }, process.env.AUTH0_SECRET, {
        expiresIn: 86400 // expires in 24 hours
      });
      ctx.body = {
        auth: true,
        token: tokenValue,
        user_id: user[0].id,
        name: user[0].name,
        email: user[0].email
      };
    } else {
      ctx.status = 404;
      ctx.body = { auth: false, token: '', message: 'Usuario no encontrado.' };
    }
    return next();
  }));
});


const routes = router.routes();
export default () => routes;
