import compose from 'koa-compose';

import user from './user';
import sync from './sync';
import login from './login';

export default () => compose([
  user(),
  sync(),
  login()
]);
